#include <iostream>
#include <math.h>

using namespace std;

int countPrimes(int);
int isPrime(int);
int theBigFibber(int, int, int);

int main()
{
	int numberNeeded = 0;
	cout << "Please enter the prime/fibonacci number needed: ";
	cin >> numberNeeded;

	cout << "The number (" << numberNeeded << ") prime number is: " << countPrimes(numberNeeded) << endl;
	cout << "The number (" << numberNeeded << ") fibonacci number is: " << theBigFibber(numberNeeded, 0, 0) << endl;

	system ("pause");
	return 0;
}

int theBigFibber(int iteration, int last, int current)
{
	if(iteration < 2) // Iteration counts backwards.
	{
		return current; // If finished adding, return answer.
	}
	if(current < 1) // Current starts at zero, next is one.
	{
		current++;
	}
	return theBigFibber(iteration - 1, current, current + last); //For one to understand recursion, one must first understand recursion
}

int isPrime(int inputNumber)
{
  for(int x = 2; x <= sqrt(static_cast<float>(inputNumber)); x++) //using the pow() function also caused the same problem I was having with sqrt() so I just used a fix that I found on stack overflow for this issue.
  {
    if(inputNumber % x == 0)
    {
      return false;
    }
  }
  return true;
}

int countPrimes(int inputNumber)
{
  int primeCount = 0;
  int prime;
  for(int i = 2; primeCount < inputNumber; i++)
  {
    if(isPrime(i))
    {
      prime = i;
      primeCount++;
    }
  }
  return prime;
}